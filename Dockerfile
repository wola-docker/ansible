FROM ubuntu:20.04

RUN apt update && \
    apt -y install ansible && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/
